**IMPORTANT:** This sample project requires a real test device, since it uses push notifications. The network calls will not work unless a device token has been retrieved.

###Constants
Please ensure that the `Constants.swift` file is populated with project specific values.


```
static let CLIENT_ID: String = {{Phoenix Application Client ID}}
static let CLIENT_SECRET: String = {{Phoenix Application Client Secret}}
static let PROJECT_ID: String = {{Phoenix Application Project Id}}
```

The following group ids can be found at `Identity > Groups` on the Phoenix Dashboard

```
enum IdentityGroup: Int {
    case Aveda = {{Segment Group Id for Aveda Users}}
    case PurePrivilege = {{Segment Group Id for Pure Privilege Users}}
    case AvedaAndPurePrivilege = {{Segment Group Id for Aveda & Pure Privilege Users}}
}
```
###Xcode Project Settings
If you're will be using your own Apple developer account to test this sample project, please ensure that you register another bundle identifier other than was committed to this project `com.tigerspike.Phoenix-User-Segmentation` and ensure that the push notification entitlement is enabled.

###Phoenix Dashboard Setup
When you have a generated APN `.p12` certificate, create a new messaging account under `Messaging > Accounts` on the Phoenix Dashbaord, setting the  gateway to `Sandbox` if you are testing this on a development certificate. Reference this account for each segment template group `Messaging > Templates` that you will be using by updating the `AccountId` field with the account that was just made.

###Testing APNS in App
When you run the application for the first time, you **MUST** accept in app notifications. When you accept, the device will register for a token via APNS, and kick off the Phoenix services. A user is automatically registered on Phoenix. Touching each label will register the device to the respective segment. For example tapping on `Pure Privilege User` will move the user into the `Pure Privilege User` segment and remove the user from the other segments. 

To send a push notification to this user, who is a part of the `Pure Privilege User` segment, navigate to `Messaging > Broadcasts` on the Phoenix Dashboard and click on `New Broadcast`. Select the respective segment `Template Group` and `Recipient Group` and click `Create Broadcast`. 

In this example if you create a push notification for other segments, the device will not receive a notification, unless they are moved to another group after click on the respective button in-app. Each segment is mutually exclusive, meaning you can only be a part of one group at any given time.