//
//  IntelligenceNetwork.swift
//  Phoenix User Segmentation
//
//  Created by Henry Chan on 5/26/17.
//  Copyright © 2017 Henry Chan. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - Singleton

final class IntelligenceNetwork {
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let shared = IntelligenceNetwork()

    var accessToken: String?
    
    var consoleLog: String = "Started"
    
    func appendToConsole(text: String) {
        self.consoleLog = text + "\n" + self.consoleLog
    }
    
    func phoenixRequest(path:String) -> String {
        if Constants.ENVIRONMENT == "staging" {
            return "http://api.staging.phoenixplatform.com/" + path
        } else {
            return "http://api.phoenixplatform.com/" + path
        }
        
    }
    
    func oAuthTokenRequest(completion:(() -> Void)?) {
        let parameters: Parameters = [
            "client_id": Constants.CLIENT_ID,
            "client_secret": Constants.CLIENT_SECRET,
            "grant_type": "client_credentials"
        ]
        Alamofire.request(phoenixRequest(path: "identity/v1/oauth/token"), method:.post, parameters: parameters).responseJSON { response in
            switch response.result {
                case .success(let JSON):
                    let response = JSON as! NSDictionary
                    self.accessToken = response.object(forKey: "access_token") as? String
                    print("Phoenix access token: " + self.accessToken!)
                    
                    completion?()
                    break
                case .failure(let error):
                    print(error)
                    completion?()
            }
        }
    }
    func createUser(username:String?, completion:(() -> Void)?) {
        if let accessToken = self.accessToken, let username = username {
            let parameters: Parameters = [
                "FirstName" : "SDK",
                "UserTypeId" : "User",
                "CompanyId" : 9105,
                "Username" : username,
                "LockingCount" : 0,
                "IsActive" : true,
                "LastName" : "User",
                "Password" : "mi9D4AZ9n"
            ]
           
            if let url = URL(string: phoenixRequest(path:"identity/v1/projects/"+Constants.PROJECT_ID+"/users")) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.post.rawValue
                
                urlRequest.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: [parameters])
                
                Alamofire.request(urlRequest)
                    .responseJSON { response in
                        switch response.result {
                            case .success(let JSON):
                                let response = JSON as! NSDictionary
                                print("Creating user:")
                                print(response)
                                PhoenixUser.shared.setUser(response: response)
                                self.appendToConsole(text: "Hi: \(String(describing: PhoenixUser.shared.username)), your id is: \(String(describing: PhoenixUser.shared.id))")
                                completion?()
                                break
                            case .failure(let error):
                                print(error)
                                completion?()
                        }
                }
                
            }
        
        } else {
            completion?()
        }
        
    }
    func getUser(userID: String?, completion:(()->Void)?) {
        if let accessToken = self.accessToken, let userID = userID {
            if let url = URL(string: phoenixRequest(path:"identity/v1/projects/"+Constants.PROJECT_ID+"/users/"+userID)) {
                var urlRequest = URLRequest(url: url)
                
                urlRequest.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
                
                Alamofire.request(urlRequest)
                    .responseJSON { response in
                        switch response.result {
                        case .success(let JSON):
                            let response = JSON as! NSDictionary
                            print("Got user:")
                            print(response)
                            PhoenixUser.shared.setUser(response: response)
                            self.appendToConsole(text: "Hi: \(String(describing: PhoenixUser.shared.username)), your id is: \(String(describing: PhoenixUser.shared.id))")
                            completion?()
                            break
                        case .failure(let error):
                            print(error)
                            completion?()
                        }
                }
                
            }
        } else {
            completion?()
        }

    }
    func createIdentifier(identifier: String?, userID: String?, completion:(()->Void)?) {
        if let accessToken = self.accessToken, let identifier = identifier, let userID = userID {
            if let url = URL(string: phoenixRequest(path:"identity/v1/projects/"+Constants.PROJECT_ID+"/users/"+userID+"/identifiers")) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.post.rawValue
                
                urlRequest.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
                
                let parameters: Parameters = [
                    "Id" : "1",
                    "ProjectId":Constants.PROJECT_ID,
                    "UserId":userID,
                    "IdentifierTypeId":3,
                    "IsConfirmed":true,
                    "Value":identifier,
                    "ModifyDate":"2016-10-26T21:08:04.0230430+00:00"
                ]
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: [parameters], options: .prettyPrinted)
                
                Alamofire.request(urlRequest)
                    .responseJSON { response in
                        switch response.result {
                        case .success(let JSON):
                            let response = JSON as! NSDictionary
                            print(response)
                            completion?()
                            break
                        case .failure(let error):
                            print(error)
                            completion?()
                        }
                }
                
            }
        } else {
            completion?()
        }
    }
    func listMembership(userID: String?, completion:(()->Void)?) {
        if let accessToken = self.accessToken, let userID = userID {
            if let url = URL(string: phoenixRequest(path:"identity/v1/projects/"+Constants.PROJECT_ID+"/memberships?userid="+userID)) {
                var urlRequest = URLRequest(url: url)
                urlRequest.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
                
                Alamofire.request(urlRequest)
                    .responseJSON { response in
                        switch response.result {
                        case .success(let JSON):
                            let response = JSON as! NSDictionary
                            print(response)
                            PhoenixUser.shared.setMembership(response: response)
                            completion?()
                            break
                        case .failure(let error):
                            print(error)
                            completion?()
                        }
                }

                
            } else {
                completion?()
            }
        }
    }
    func createMembership(groupId: Int, userID: String?, completion:(()->Void)?) {
        if let accessToken = self.accessToken, let userID = userID {
            if let url = URL(string: phoenixRequest(path:"identity/v1/projects/"+Constants.PROJECT_ID+"/memberships")) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.post.rawValue
                
                urlRequest.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
                
                let parameters: Parameters = [
                    "Id" : 1,
                    "GroupId" : groupId,
                    "UserId" : userID,
                    "JoinDate" : "2017-05-30T15:22:10.7594749+00:00",
                    "ExpiryDate" : "2018-05-30T15:22:10.7594749+00:00",
                    "ModifyDate" : "2017-05-30T15:22:10.7594749+00:00"                ]
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: [parameters], options: .prettyPrinted)
                
                Alamofire.request(urlRequest)
                    .responseJSON { response in
                        switch response.result {
                        case .success(let JSON):
                            let response = JSON as! NSDictionary
                            print(response)
                            completion?()
                            break
                        case .failure(let error):
                            print(error)
                            completion?()
                        }
                }
                
            }

        } else {
            completion?()
        }
    }
    func updateMembership(userID: String?, valid: Bool = true, groupID: Int, id: Int, completion:(()->Void)?) {
        if let accessToken = self.accessToken, let userID = userID {
            if let url = URL(string: phoenixRequest(path:"identity/v1/projects/"+Constants.PROJECT_ID+"/users/"+userID+"/memberships")) {
                var urlRequest = URLRequest(url: url)
                urlRequest.httpMethod = HTTPMethod.put.rawValue
                
                urlRequest.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
                urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
                
                var expiryDate = Constants.FUTURE_DATE
                if !valid {
                    expiryDate = Constants.PAST_DATE
                }
                let parameters: Parameters = [
                    "Id" : id,
                    "GroupId" : groupID,
                    "UserId" : userID,
                    "JoinDate": "2017-05-30T15:22:10.76",
                    "ExpiryDate": expiryDate,
                    "ModifyDate": "2017-05-30T15:45:51.963"        ]
                urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: [parameters], options: .prettyPrinted)
                
                Alamofire.request(urlRequest)
                    .responseJSON { response in
                        switch response.result {
                        case .success(let JSON):
                            let response = JSON as! NSDictionary
                            print(response)
                            completion?()
                            break
                        case .failure(let error):
                            print(error)
                            completion?()
                        }
                }
                
            }
            
        } else {
            completion?()
        }

    }
}
