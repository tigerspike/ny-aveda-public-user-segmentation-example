//
//  AppDelegate.swift
//  Phoenix User Segmentation
//
//  Created by Henry Chan on 5/26/17.
//  Copyright © 2017 Henry Chan. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else{
                    //Do stuff if unsuccessful...
                }
            })
        } else {
            // Fallback on earlier versions
        }
        
        return true
    }

 
    // Handle remote notification registration.
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
        // Forward the token to your provider, using a custom method.
        let deviceToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Device token: " + deviceToken)
        IntelligenceNetwork.shared.oAuthTokenRequest {
            if let userID = KeychainService.loadUserID() as String? {
                IntelligenceNetwork.shared.getUser(userID: userID, completion: {
                    IntelligenceNetwork.shared.createIdentifier(identifier: deviceToken, userID: PhoenixUser.shared.id, completion: nil)
                })
            } else {
                IntelligenceNetwork.shared.createUser(username: UUID().uuidString, completion: {
                    KeychainService.saveUserID(userID: PhoenixUser.shared.id! as NSString)
                    IntelligenceNetwork.shared.createIdentifier(identifier: deviceToken, userID: PhoenixUser.shared.id, completion: nil)
                })
            }
            
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        if application.applicationState == .active {
            if let aps = userInfo["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSDictionary {
                    if let _ = alert["message"] as? NSString {
                        //Do stuff
                    }
                } else if let alertMessage = aps["alert"] as? NSString {
                    //Do stuff
                    let alert = UIAlertController(title: "Push Notification", message: String(alertMessage), preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Okay", style:.cancel, handler:nil))
                    
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
            }
            
            
            

        }
        
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // The token is not currently available.
        print("Remote notification support is unavailable due to error: \(error.localizedDescription)")
       
    }
    
    

}


