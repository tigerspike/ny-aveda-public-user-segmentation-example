//
//  PhoenixUser.swift
//  Phoenix User Segmentation
//
//  Created by Henry Chan on 5/26/17.
//  Copyright © 2017 Henry Chan. All rights reserved.
//

import Foundation

class PhoenixUser {
    
    var username:String?
    var id: String?
    var membership: [IdentityGroup:Int]?
    
    private init() { }
    
    // MARK: Shared Instance
    
    static let shared = PhoenixUser()

    
    func setUser(response: NSDictionary) {
        if let data = (((response.object(forKey: "Data") as? NSArray)?.firstObject) as? NSDictionary) {
            
            self.username = data.object(forKey: "Username") as? String
            if let test = (data.object(forKey: "Id") as? NSNumber)?.intValue {
                let string = String(test)
                self.id = string
            }
            
        }
        
    }
    func setMembership(response: NSDictionary) {
        if let data = (response.object(forKey: "Data") as? NSArray) {
            membership = [:]
            for group in data {
                if let group = group as? NSDictionary {
                    if let key = IdentityGroup(rawValue: (group.object(forKey: "GroupId") as! Int)) {
                        self.membership?[key] = group.object(forKey: "Id") as? Int
                    }
                    
                }
            }
        }
    }
}
