//
//  DetailViewController.swift
//  Phoenix User Segmentation
//
//  Created by Henry Chan on 5/26/17.
//  Copyright © 2017 Henry Chan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var consoleTextView: UITextView!

    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    func configureView() {
        // Update the user interface for the detail item.
        consoleTextView.text = IntelligenceNetwork.shared.consoleLog
        loadingIndicator.isHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    @IBAction func avedaUserTapped(_ sender: UIButton) {
        setUserSegment(groupId: IdentityGroup.Aveda)
        
    }

    @IBAction func purePrivilegeUserTapped(_ sender: UIButton) {
        setUserSegment(groupId: IdentityGroup.PurePrivilege)
    }
    
    @IBAction func avedaAndPPUserTapped(_ sender: Any) {
        setUserSegment(groupId: IdentityGroup.AvedaAndPurePrivilege)
    }
    
    func startLoading() {
        button3.isEnabled = false
        button2.isEnabled = false
        button1.isEnabled = false
        loadingIndicator.startAnimating()
        loadingIndicator.isHidden = false
    }
    
    func stopLoading() {
        button3.isEnabled = true
        button2.isEnabled = true
        button1.isEnabled = true
        loadingIndicator.stopAnimating()
        loadingIndicator.isHidden = true
    }
    func setUserSegment(groupId:IdentityGroup) {
        startLoading()
        IntelligenceNetwork.shared.listMembership(userID: PhoenixUser.shared.id) {
            if let membership = PhoenixUser.shared.membership {
                var membershipExists = false
                for group in membership {
                    let id = membership[group.key]!
                    if groupId == group.key {
                        membershipExists = true
                        
                        IntelligenceNetwork.shared.updateMembership(userID: PhoenixUser.shared.id, valid: true, groupID: group.key.rawValue, id: id, completion: {
                            self.stopLoading()
                            IntelligenceNetwork.shared.appendToConsole(text: "Added user \(PhoenixUser.shared.id!) to: \(groupId)")
                            self.consoleTextView.text = IntelligenceNetwork.shared.consoleLog
                        })
                    } else {
                        IntelligenceNetwork.shared.updateMembership(userID: PhoenixUser.shared.id, valid: false, groupID: group.key.rawValue, id: id, completion: {
                            self.stopLoading()
                            IntelligenceNetwork.shared.appendToConsole(text: "Removed user \(PhoenixUser.shared.id!) from: \(group.key)")
                            self.consoleTextView.text = IntelligenceNetwork.shared.consoleLog
                        })
                    }
                }
                
                if !membershipExists {
                    IntelligenceNetwork.shared.createMembership(groupId: groupId.rawValue, userID: PhoenixUser.shared.id, completion: {
                        self.stopLoading()
                        IntelligenceNetwork.shared.appendToConsole(text: "Added user \(PhoenixUser.shared.id!) to: \(groupId)")
                        self.consoleTextView.text = IntelligenceNetwork.shared.consoleLog
                    })
                }
            }
            
        }
        
    }
}

