//
//  Constants.swift
//  Phoenix User Segmentation
//
//  Created by Henry Chan on 5/30/17.
//  Copyright © 2017 Henry Chan. All rights reserved.
//

struct Constants {
    
    static let CLIENT_ID: String =
    static let CLIENT_SECRET: String =
    static let PROJECT_ID: String = 
    static let ENVIRONMENT: String = "production"// staging or production
    
    static let FUTURE_DATE = "2019-05-30T15:22:10.76" // This should be a future valid date
    static let PAST_DATE = "2014-05-30T15:22:10.76" // A past date from now, indicates that they won't receive the notification
    
}

enum IdentityGroup: Int {
    case Aveda =
    case PurePrivilege =
    case AvedaAndPurePrivilege =
}
